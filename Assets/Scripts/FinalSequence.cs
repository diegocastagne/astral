﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalSequence : MonoBehaviour
{
    [SerializeField] private UnlockTheDoor _unlock = null;

    void Start()
    {
        StartCoroutine(CountDown(1.5f));
    }

    private IEnumerator CountDown(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        _unlock.UnlockDoor();
    }

}
