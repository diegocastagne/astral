﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _sensitivity = 1.0f;
    [SerializeField] private float _moveSpeed = 5.0f;
    [SerializeField] private Camera _playerCamera = null;
    [SerializeField] private float _maxCameraAngle = 90f;
    [SerializeField] private GameObject _pressEText = null; // Change it to be an actual Canvas (for now it's alright)
    [SerializeField] public GameObject lifelessBody = null;
    [SerializeField] private GameObject _physicalArms = null;
    [SerializeField] private FixedJoint _pickupJoint = null;

    [SerializeField] private TextMeshProUGUI _timeText = null;

    [Header("Running")]
    [SerializeField] private float _runMultiplier = 1.5f;
    [SerializeField] private LayerMask _groundLayerMask = default;
    [SerializeField] private float _staminaTime = 2f;
    [SerializeField] private float _restingTime = 4f;

    [Header("SFX")]
    [SerializeField] AudioMixerSnapshot physicalSnapshot = null;
	[SerializeField] AudioMixerSnapshot astralSnapshot = null;

    [Header("UI")]
    [SerializeField] private Image _etherealUI = null;
    [SerializeField] Image fadeIn = null;
    
    [Header("This is for debug purposes, do not change manually")]
    [SerializeField] private Interactable _objectToInteractWith = null;
    private bool _objectOnSightIsPickup;
    private Rigidbody _rigidBody;
    private float _moveForward = 0f;
    private float _moveSides = 0f;
    private bool _interactableOnSight = false;
    private Coroutine _moveToRoutine;
    private float _timePlaceHolder = 3f;
    private Quaternion _cameraCenter;
    private SoundSlidingDoor _whooshSound = null;
    public bool _isRunning = false;
    private float _timerStamina = 0f;

    public bool _isGrounded = false;

    public bool isInteracting = false;
    public bool paused = false;
    public bool isInEtherealForm = false;
    public GameObject gameOverScreen = null;
    public bool lost = false;
    private PickUp _pickupTemp;
    private TextMeshProUGUI _textInteractionReference;

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _cameraCenter = _playerCamera.transform.rotation;
        _whooshSound = GetComponent<SoundSlidingDoor>();
        physicalSnapshot.TransitionTo(0.01f);
        StartCoroutine(FadeIn());
        _textInteractionReference = _pressEText.GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        if(_objectToInteractWith != null && !isInteracting && _interactableOnSight)
            _pressEText.SetActive(true);
        else
            _pressEText.SetActive(false);

        if(isInteracting && _objectToInteractWith!=null && _objectToInteractWith._isBeingInteractedWith == false)
        {
            AbortInteraction();
            AbortInteraction();
        }

        if(_pickupTemp != null)
        {
            if(_pickupTemp.isBeingHeld)
            {
                if(_pickupTemp.canBePlaced)
                    _textInteractionReference.text = "Press E to place";
                else
                    _textInteractionReference.text = "Press E to drop";

            }
            
        }

        Debug.Log(_timerStamina);
    }
    
    private void FixedUpdate() {
        Vector3 movingAngle = new Vector3(_moveSides, _rigidBody.velocity.y, _moveForward);

        if(_isRunning&&_isGrounded)
        {
            movingAngle.x *=  _runMultiplier;
            movingAngle.z *= _runMultiplier;

            _timerStamina += Time.fixedDeltaTime;

            if(_timerStamina>_staminaTime)
                _isRunning = false;
        }
        else
        {
            if(_timerStamina > 0f)
                _timerStamina -= Time.fixedDeltaTime / (_restingTime/_staminaTime);
            else
                _timerStamina = 0f;
        }

        movingAngle = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0) * movingAngle;
        _rigidBody.velocity = movingAngle;

        _isGrounded = Physics.Raycast(transform.position - (transform.up * 1.4f), -transform.up, 0.3f, _groundLayerMask);
        Debug.DrawRay(transform.position - (transform.up*1.4f), -transform.up*0.3f, Color.red);

    }

    private void OnTriggerStay(Collider other)
    {
        if((other.CompareTag("Interactable") && !isInEtherealForm) || (other.CompareTag("InteractableEthereal") && isInEtherealForm))
        {
            _interactableOnSight = true;
            if(!_objectToInteractWith)
            {
                _objectToInteractWith = other.gameObject.GetComponent<Interactable>();
                _objectOnSightIsPickup = false;
                _textInteractionReference.text = "Press E to Interact";
            }
        }

        if((other.CompareTag("Pickup") && !isInEtherealForm))
        {
            _interactableOnSight = true;
            if(!_objectToInteractWith)
            {
                _objectToInteractWith = other.gameObject.GetComponent<Interactable>();
                _objectOnSightIsPickup = true;
                _textInteractionReference.text = "Press E to Pick up";
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Interactable") || other.CompareTag("InteractableEthereal") || other.CompareTag("Pickup"))
        {
            // add a check to see if it's the same object it's interacting with
            if(_objectToInteractWith != null && other.gameObject == _objectToInteractWith.gameObject)
            {
                _interactableOnSight = false;
                _objectToInteractWith.AbortInteraction();
                AbortInteraction();
                AbortInteraction();
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.collider.CompareTag("Enemy"))
            Die();
    }

    public void MoveCamera(float rotateX, float rotateY)
    {
        if(!isInteracting && !paused)
        {
            rotateX *= _sensitivity;
            rotateY *= _sensitivity;

            transform.Rotate(0f, rotateX, 0f);
            _cameraCenter *= Quaternion.Euler(0f, rotateX, 0f);
            Quaternion yQuaternion = Quaternion.AngleAxis(rotateY, -Vector3.right);
            Quaternion temp = _playerCamera.transform.rotation * yQuaternion;
            float angleBetween = Quaternion.Angle(_cameraCenter, temp);

            if(angleBetween < _maxCameraAngle)
            {
                _playerCamera.transform.rotation = temp;
            } 
        }
    }

    public void TryMove(float forwardInput, float sidesInput)
    {
        if(!isInteracting)
        {
            _moveForward = forwardInput * _moveSpeed;
            _moveSides = sidesInput * _moveSpeed;
        }
        else
        {
            _objectToInteractWith.Move(forwardInput, sidesInput);
        }
    }

    public void TryRun(bool shouldRun)
    {
        _isRunning = shouldRun;
    }

    public void TryInteract()
    {
        if(_objectToInteractWith != null && _interactableOnSight && !isInteracting && !paused && !_objectOnSightIsPickup)
        {
            _moveForward = 0f;
            _moveSides = 0f;
            isInteracting = _objectToInteractWith.Interact(); //the method returns a bool if it needs to start an interaction or it's a one step thing
        }
        else if(_objectToInteractWith != null && _interactableOnSight && !isInteracting && !paused)
        {
            if(_pickupTemp == null)
            {
                _pickupTemp = _objectToInteractWith.gameObject.GetComponent<PickUp>();
            }

            if(!_pickupTemp.isBeingHeld)
            {
                _objectToInteractWith.transform.position = _pickupJoint.transform.position;
                _pickupJoint.connectedBody = _objectToInteractWith.GetComponent<Rigidbody>();
            }
            else
            {
                _pickupJoint.connectedBody = null;
                _pickupTemp = null;
            }
            isInteracting = _objectToInteractWith.Interact();
        }
    }

    public void AbortInteraction()
    {
        if(_objectToInteractWith != null && isInteracting && !paused)
        {
            _objectToInteractWith.AbortInteraction();
            isInteracting = false;
        }else if(_objectToInteractWith != null)
        {
            _objectToInteractWith = null;
        }
    }

    public void SwapForm()
    {

        if(isInEtherealForm)
        {
            _physicalArms.SetActive(true);
            _timeText.gameObject.SetActive(false);
            StopAllCoroutines();
            AbortInteraction();
            AbortInteraction();
            _rigidBody.mass = 1f;
            MoveTo(lifelessBody.transform.position, lifelessBody.transform.rotation);
            lifelessBody.SetActive(false);
            isInEtherealForm = false;
            lifelessBody.GetComponent<LifelessBody>().playerHasExited = false; //very bad practice
            _etherealUI.fillAmount = 1;
            _whooshSound.PlayClose();
            _playerCamera.GetComponent<UnityEngine.Rendering.PostProcessing.PostProcessLayer>().enabled = false;
            physicalSnapshot.TransitionTo(0.5f);
        }
        else
        {
            _physicalArms.SetActive(false);
            //_timeText.gameObject.SetActive(true);
            AbortInteraction();
            AbortInteraction();
            _rigidBody.mass = 0f;
            lifelessBody.SetActive(true);
            lifelessBody.transform.position = transform.position;
            lifelessBody.transform.rotation = transform.rotation;
            Vector3 temp = transform.position - (transform.forward * 1.5f);
            MoveTo(temp, transform.rotation);
            isInEtherealForm = true;
            _whooshSound.PlayClose();
            _playerCamera.GetComponent<UnityEngine.Rendering.PostProcessing.PostProcessLayer>().enabled = true;
            astralSnapshot.TransitionTo(0.5f);
        }
    }

    private void MoveTo(Vector3 targetPosition, Quaternion targetRotation)
    {
        if(_moveToRoutine != null) 
            StopCoroutine(_moveToRoutine);

        _moveToRoutine = StartCoroutine(MoveToRoutine(targetPosition, targetRotation));

        _cameraCenter = targetRotation;
    }

    private IEnumerator MoveToRoutine(Vector3 targetPosition, Quaternion targetRotation)
    {
        gameObject.layer = 12;
        Vector3 origin = transform.position;
        Quaternion originRotation = transform.rotation;
        float progress = 0;
        float step = 2.2f;

        while (progress < 0.995f)
        {
            transform.position = Vector3.Lerp(origin, targetPosition, -progress * (progress - 2));
            transform.rotation = Quaternion.Lerp(originRotation, targetRotation, -progress * (progress - 2));
            progress += step * Time.deltaTime;
            yield return null; /// waits one update() frame
        }

        transform.position = targetPosition;
        _moveToRoutine = null;
        gameObject.layer = 11;
    }

    private IEnumerator Timer(float seconds)
    {
        float duration = seconds; 
        float normalizedTime = 0;
        while(normalizedTime <= 1f)
        {
            //_timeText.text = "" + (_timePlaceHolder - (normalizedTime * duration));
            normalizedTime += Time.deltaTime / duration;

            _etherealUI.fillAmount = 1 - normalizedTime;
            yield return null;
        }

        _etherealUI.fillAmount = 1;
    }

    public void StartTimer(float seconds)
    {
        StartCoroutine(Timer(seconds));
    }

    public void Die()
    {
        if(!lost)
        {
            gameOverScreen.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            StartCoroutine(FadeOut());

            lost = true;
        }
    }

    private IEnumerator FadeOut()
    {
        Image fadeOut = gameOverScreen.GetComponentInChildren<Image>();
        float duration = 0.3f;
        float normalizedTime = 0;
        while(normalizedTime <= 1f)
        {
            fadeOut.color = new Color(0f, 0f, 0f, normalizedTime);
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }

        fadeOut.color = new Color(0f, 0f, 0f, 1f);
        //Time.timeScale = 0f;
        // gameObject.layer = 12;
        // lifelessBody.layer = 12;
    }

    private IEnumerator FadeIn()
    {
        float duration = 0.3f;
        float normalizedTime = 0;
        while(normalizedTime <= 1f)
        {
            fadeIn.color = new Color(0f, 0f, 0f, 1f - normalizedTime);
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }

        fadeIn.color = new Color(0f, 0f, 0f, 0f);

        //Time.timeScale = 0f;
        // gameObject.layer = 12;
        // lifelessBody.layer = 12;
    }

    public void MoveLifelessBodyTo(Transform target)
    {
        lifelessBody.transform.position = target.position;
        lifelessBody.transform.rotation = target.rotation;
    }
}
