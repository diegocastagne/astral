using System.Collections.Generic;
using UnityEngine;

public class EtherealPenetrable : MonoBehaviour
{
    public static List<EtherealPenetrable> list;

    private void Awake()
    {
        if(list == null)
            list = new List<EtherealPenetrable>();
            
        list.Add(this);
    }

    public void SwapLayers()
    {
        if(gameObject.layer == 9)
            gameObject.layer = 10;
        else
            gameObject.layer = 9;
    }

}