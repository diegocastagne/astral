﻿using UnityEngine;

public class ChangeMusicOnScene : MonoBehaviour
{
    [SerializeField] private AudioClip _thisLevelMusic = null;

    private void Awake()
    {
        if(MusicPlayer.Instance != null)
        {
            MusicPlayer.Instance.StopMusic();
            MusicPlayer.Instance.ChangeClip(_thisLevelMusic);
            MusicPlayer.Instance.PlayMusic();
        }

    }
}
