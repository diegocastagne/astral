﻿using System.Collections;
using UnityEngine;

public class GateButton : Interactable
{
    [SerializeField] private GameObject _gateToToggle = null;

    private bool _gateIsOpen = false;
    private bool _gateIsMoving = false;

    [SerializeField] private Material _notAvailableMat = null;
    [SerializeField] private Material _availableMat = null;
    private Renderer _rend = null;

    private Coroutine _moveToRoutine;
    [SerializeField] private float _deltaY = 0f;
    [SerializeField] private float _deltaX = 0f;

    [SerializeField] SoundSlidingDoor _sound = null;

    private void Awake()
    {
        _rend = GetComponent<Renderer>();
    }

    private void Update()
    {
        _rend.material = _gateIsMoving ? _notAvailableMat : _availableMat;
    }

    private void MoveGateTo(Vector3 targetPosition)
    {
        if(_moveToRoutine != null) 
            StopCoroutine(_moveToRoutine);

        _moveToRoutine = StartCoroutine(MoveToRoutine(targetPosition));

    }

    private IEnumerator MoveToRoutine(Vector3 targetPosition)
    {
        _gateIsMoving = true;
        Vector3 origin = _gateToToggle.transform.position;
        float progress = 0;
        float step = 1f;

        while (progress < 0.995f)
        {
            _gateToToggle.transform.position = Vector3.Lerp(origin, targetPosition, -progress * (progress - 2));
            progress += step * Time.deltaTime;
            yield return null; /// waits one update() frame
        }

        _gateToToggle.transform.position = targetPosition;
        _gateIsMoving = false;
        _moveToRoutine = null;
    }

    public override bool Interact()
    {
        if(_gateIsMoving) return false;

        Vector3 target = _gateToToggle.transform.position;
        if(_gateIsOpen)
        {
            _sound.PlayClose();
            target.y -= _deltaY;
            target.x -= _deltaX;
            _gateIsOpen = false;
        }
        else
        {
            _sound.PlayOpen();
            target.y += _deltaY;
            target.x += _deltaX;
            _gateIsOpen = true;
        }

        MoveGateTo(target);

        return false;
    }

    public override void AbortInteraction()
    {
        // will not need this
    }


    public override void Move(float y, float x)
    {
        // will not need this
    }
}
