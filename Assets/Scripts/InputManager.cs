﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    [SerializeField] private string _frontBackInput = "";
    [SerializeField] private string _sidesInput = "";
    [SerializeField] private string _xCameraInput = "";
    [SerializeField] private string _yCameraInput = "";
    [SerializeField] PlayerController _player = null;
    [SerializeField] private float _timeEtherealForm = 5f;

    //[SerializeField] private Image _etherealUI = null;

    public bool lockQ = false;


    private GameManager _gameManager;

    private void Awake()
    {
        _gameManager = GetComponent<GameManager>();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if(_player.lost) return;

        if(Input.GetKeyDown(KeyCode.Q))
        {
            if(!lockQ)
                Swap();
        }

        if(Input.GetKeyDown(KeyCode.V))
        {
            EtherealPenetrable.list.Clear();
            EtherealVisible.list.Clear();
            SceneManager.LoadScene("Tutorial");
        }

        if(Input.GetKeyDown(KeyCode.B))
        {
            EtherealPenetrable.list.Clear();
            EtherealVisible.list.Clear();
            SceneManager.LoadScene("TheHallway");
        }

        if(Input.GetKeyDown(KeyCode.N))
        {
            EtherealPenetrable.list.Clear();
            EtherealVisible.list.Clear();
            SceneManager.LoadScene("TheLab");
        }

        if(Input.GetKeyDown(KeyCode.M))
        {
            EtherealPenetrable.list.Clear();
            EtherealVisible.list.Clear();
            SceneManager.LoadScene("M_MainMenu");
        }

        if(Input.GetKeyDown(KeyCode.E))
        {
            if(_player.isInteracting)
            {
                _player.AbortInteraction();
            }
            else
            {
                _player.TryInteract();
            }
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!_player.paused)
            {
                _gameManager.Pause();
                _player.paused = true;
            }
            else
            {
                _gameManager.Resume();
                _player.paused = false;
            }
        }

        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            _player.TryRun(true);
        }

        if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            _player.TryRun(false);
        }

        float rotateX = Input.GetAxis(_xCameraInput);
        float rotateY = Input.GetAxis(_yCameraInput);
        _player.MoveCamera(rotateX, rotateY);

        float moveForward =  Input.GetAxis(_frontBackInput);
        float moveSides =  Input.GetAxis(_sidesInput);

        Vector2 inputVector = MapToCircle(new Vector2(moveSides, moveForward));

        _player.TryMove(inputVector.y, inputVector.x);
    }

    private Vector3 MapToCircle(Vector3 vector)
    {
        return new Vector3(vector.x * Mathf.Sqrt(1 - vector.y * vector.y / 2), vector.y * Mathf.Sqrt(1 - vector.x * vector.x / 2), 0f);
    }

    public void Swap()
    {
        if(!_player.isInEtherealForm)
        {
            StartCoroutine(SwapRoutine());
        }
        else
        {
            StopAllCoroutines();
        }
        _gameManager.SwapObjects();
        _player.SwapForm();
    }

    public void SwapWithoutRoutines()
    {
        _gameManager.SwapObjects();
        _player.SwapForm();
    }

    private IEnumerator SwapRoutine()
    {
        if(!_player.isInEtherealForm)
            _player.StartTimer(_timeEtherealForm);
            
        yield return new WaitForSeconds(_timeEtherealForm);
        _gameManager.SwapObjects();


        _player.SwapForm();
    }

    private void Respawn()
    {
        if(!_player.gameObject.activeSelf)
        {
            _player.gameObject.SetActive(true);
            _player.gameOverScreen.SetActive(false);
        }
        _player.gameObject.transform.position = new Vector3(0f, 2f, 0f);
    }
}
