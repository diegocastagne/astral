﻿using System.Collections;
using UnityEngine;

public class UnlockTheDoor : MonoBehaviour
{
    [SerializeField] private GameObject _doorLeft = null;
    [SerializeField] private GameObject _doorRight = null;
    [SerializeField] private bool _requiresElectricity = true;
    [SerializeField] private Cable _activatingCable = null;
    [SerializeField] private SoundPlayer _sound = null;

    private Coroutine _moveToRoutine;
    private Coroutine _moveToRoutine2;
    [SerializeField] private float _deltaY = 0f;
    [SerializeField] private float _deltaX = 0f;
    [SerializeField] private float _deltaZ = 0f;

    private bool _hasBeenUnlocked = false;

    private void Update()
    {
        if(!_requiresElectricity) return;
        
        if(_activatingCable.hasElectricity && !_hasBeenUnlocked)
        {
            UnlockDoor();
            _hasBeenUnlocked = true;
        }
    }

    public void UnlockDoor()
    {
        //_door.SetActive(false);

        Vector3 target = _doorLeft.transform.position;
        target.y += _deltaY;
        target.x += _deltaX;
        target.z += _deltaZ;

        MoveGatesTo(target);

        
        if(_sound != null)
            _sound.PlayMusic();
    }

    private void MoveGatesTo(Vector3 targetPosition)
    {
        if(_moveToRoutine != null) 
            StopCoroutine(_moveToRoutine);
        if(_moveToRoutine2 != null) 
            StopCoroutine(_moveToRoutine2);

        _moveToRoutine = StartCoroutine(MoveToRoutine(targetPosition, _doorLeft));

        Vector3 rightTarget = _doorRight.transform.position;
        rightTarget.y += _deltaY * -1f;
        rightTarget.x += _deltaX * -1f;
        rightTarget.z += _deltaZ * -1f;
        _moveToRoutine2 = StartCoroutine(MoveToRoutine(rightTarget, _doorRight));

    }

    private IEnumerator MoveToRoutine(Vector3 targetPosition, GameObject doorToMove)
    {
        Vector3 origin = doorToMove.transform.position;
        float progress = 0;
        float seconds = 3f;
        float step = 1f/seconds;

        while (progress < 0.995f)
        {
            doorToMove.transform.position = Vector3.Lerp(origin, targetPosition, -progress * (progress - 2));
            progress += step * Time.deltaTime;
            yield return null; /// waits one update() frame
        }

        doorToMove.transform.position = targetPosition;
        _moveToRoutine = null;
    }
}
