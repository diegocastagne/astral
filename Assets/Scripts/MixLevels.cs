﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using UnityEngine.Audio;

public class MixLevels : MonoBehaviour {

    [Range(-80f, 0f)]
    public float volume = 0f;

    [SerializeField] Slider sfxSlider = default;
    [SerializeField] Slider musicSlider = default;

    [SerializeField] AudioMixer mixer = default;

    //TODO: IMPLEMENT SFX AND MUSIC VOLUME SLIDER FUNCTIONALITY

    public void SetSfxLvl()
	{
        mixer.SetFloat("sfxVolume", AudioUtilities.linearToDecibel(sfxSlider.value));
    }

    public void SetMusicLvl ()
	{
        mixer.SetFloat("musicVolume", AudioUtilities.linearToDecibel(musicSlider.value));
    }
}
