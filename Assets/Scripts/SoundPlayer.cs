﻿using UnityEngine;
 using System.Collections;
 
 public class SoundPlayer : MonoBehaviour
 {
    [SerializeField] private AudioSource _audioSource = null;
    [SerializeField] private AudioClip[] audioClips = { };

    void Awake()
    {
        ChangeClip(audioClips[0]);
    }

    public void PlayMusic()
    {
        if (_audioSource.isPlaying) return;

        // Audio Clip randomization
        ChangeClip(audioClips[Random.Range(0, audioClips.Length)]);
        
        _audioSource.Play();
    }

    public void StopMusic()
    {
        _audioSource.Stop();
    }

    public void ChangeClip(AudioClip newClip)
    {
        _audioSource.clip = newClip;
    }
}
