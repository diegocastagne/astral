﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class KeyPadInteract : Interactable
{
    [SerializeField] private GameObject _keyPadDisplay = null; 
    [SerializeField] private GameObject _gateToOpen = null; 

    [SerializeField] private string _code = "";

    [SerializeField] private TextMeshProUGUI _inputText = null;
    [SerializeField] private Image _inputImage = null;
    [SerializeField] private bool _needsElectricity = false;
    [SerializeField] private Cable _feedingCable = null;

    [SerializeField] private SoundKeyPad _keypadSound = null;

    [SerializeField] private bool _isBigDoor = false;
    [SerializeField] private GameObject _doorRight = null;

    private Coroutine _moveToRoutine;
    private Coroutine _moveToRoutine2;
    [SerializeField] private float _deltaY = 0f;
    [SerializeField] private float _deltaX = 0f;
    [SerializeField] private float _deltaZ = 0f;

    private string _currentGuess = "";
    private bool _canInput = true;

    [SerializeField] SoundSlidingDoor _sound = null;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha9))
        {
            CloseGate();
        }

        DisplayGuessCode();
    }

    private void DisplayGuessCode()
    {
        int unusedSpace = _code.Length - _currentGuess.Length;

        string displayText = "";

        for(int i = 0; i < _currentGuess.Length; i++)
        {
            displayText += _currentGuess[i];

            if(i != _code.Length - 1)
                displayText += " ";
        }

        _inputText.text = displayText;

        for(int i = 0; i < unusedSpace; i++)
        {
            _inputText.text += "*";
            if(i != _code.Length - 1)
                _inputText.text += " ";
        }
    }

    // Here goes handling the interaction
    override public bool Interact()
    {
        if(_needsElectricity && !_feedingCable.hasElectricity) return false;

        if(!_keyPadDisplay.activeSelf)
        {
            _keyPadDisplay.SetActive(true); 
        }

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        _isBeingInteractedWith = true;

        return true;
    }

    override public void AbortInteraction()
    {
        if(_keyPadDisplay.activeSelf)
        {
            _keyPadDisplay.SetActive(false); 
        }

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        _isBeingInteractedWith = false;
    }

    public void AppendToCode(string newNumber)
    {
        if(_canInput)
        {
            _currentGuess += newNumber;

            if(_currentGuess.Length >= _code.Length)
            {
                StartCoroutine(CheckForCode(_currentGuess));
            }
        }
    }


    private IEnumerator CheckForCode(string guess)
    {
        if(guess == _code)
        {
            _inputImage.color = new Color(0f,1f,0f,0.4039216f);
            _keypadSound.PlayCorrect();
        }
        else
        {
            _inputImage.color = new Color(1f,0f,0f,0.4039216f);
            _keypadSound.PlayIncorrect();
        }

        _canInput = false;
        yield return new WaitForSeconds(0.5f);

        if(guess == _code)
        {
            OpenGate();
            AbortInteraction();
        }

        _inputImage.color = new Color(0.1129405f,0.5882353f,0.8867924f,0.4039216f);
        _currentGuess = "";
        _canInput = true;
    }

    public void OpenGate()
    {
        Vector3 target = _gateToOpen.transform.position;
        target.y += _deltaY;
        target.x += _deltaX;
        target.z += _deltaZ;

        _sound.PlayOpen();

        if(_isBigDoor)
            MoveGatesTo(target);
        else
            MoveGateTo(target);
        
        // if(_gateToOpen.activeSelf)
        // {
        //     _gateToOpen.SetActive(false);
        // }
    }

    public void CloseGate()
    {
        // if(!_gateToOpen.activeSelf)
        // {
        //     _gateToOpen.SetActive(true);
        // }
    }

    private void MoveGatesTo(Vector3 targetPosition)
    {
        if(_moveToRoutine != null) 
            StopCoroutine(_moveToRoutine);
        if(_moveToRoutine2 != null) 
            StopCoroutine(_moveToRoutine2);

        _moveToRoutine = StartCoroutine(MoveToRoutine(targetPosition, _gateToOpen));

        Vector3 rightTarget = _doorRight.transform.position;
        rightTarget.y += _deltaY * -1f;
        rightTarget.x += _deltaX * -1f;
        rightTarget.z += _deltaZ * -1f;
        _moveToRoutine2 = StartCoroutine(MoveToRoutine(rightTarget, _doorRight));

    }

    private void MoveGateTo(Vector3 targetPosition)
    {
        if(_moveToRoutine != null) 
            StopCoroutine(_moveToRoutine);

        _moveToRoutine = StartCoroutine(MoveToRoutine(targetPosition, _gateToOpen));

    }

    private IEnumerator MoveToRoutine(Vector3 targetPosition, GameObject doorToMove)
    {
        Vector3 origin = doorToMove.transform.position;
        float progress = 0;
        float seconds = _isBigDoor ? 3f : 1f;
        float step = 1f/seconds;

        while (progress < 0.995f)
        {
            doorToMove.transform.position = Vector3.Lerp(origin, targetPosition, -progress * (progress - 2));
            progress += step * Time.deltaTime;
            yield return null; /// waits one update() frame
        }

        doorToMove.transform.position = targetPosition;
        _moveToRoutine = null;
    }

    public override void Move(float y, float x)
    {
    }
}
