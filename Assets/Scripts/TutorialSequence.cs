﻿using UnityEngine;

public class TutorialSequence : MonoBehaviour
{
    [SerializeField] private InputManager _input = null;
    [SerializeField] private PlayerController _player = null;
    [SerializeField] private Transform _lifelessBodyLocation = null;
    [SerializeField] private LifelessBody _lifelessBody = null;
    [SerializeField] private GameObject _playerTimer = null;
    [SerializeField] private GameObject _playerScreen = null;

    private bool endedSequence = false;

    private void Start()
    {
        _input.SwapWithoutRoutines();

        _player.MoveLifelessBodyTo(_lifelessBodyLocation);

        _lifelessBody.playerHasExited = true;

        _playerTimer.SetActive(false);

        _input.lockQ = true;

        _playerScreen.SetActive(false);
    }

    private void Update()
    {
        if(endedSequence) return;

        if(!_player.isInEtherealForm)
        {
            _input.lockQ = false;
            _playerScreen.SetActive(true);
            endedSequence = true;
        }
    }
}
