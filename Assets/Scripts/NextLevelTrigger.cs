﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NextLevelTrigger : MonoBehaviour
{
    [SerializeField] private string _sceneToLoad = null;

    [SerializeField] Image fade = null;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(FadeOut());
            EtherealPenetrable.list.Clear();
            EtherealVisible.list.Clear();
            
        }
    }

    private IEnumerator FadeOut()
    {
        float duration = 0.3f;
        float normalizedTime = 0;
        while(normalizedTime <= 1f)
        {
            fade.color = new Color(0f, 0f, 0f, normalizedTime);
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }
        fade.color = new Color(0f, 0f, 0f, 1f);
        SceneManager.LoadScene(_sceneToLoad);
    }
}
