﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;


public class Patrol : MonoBehaviour {

    public Transform[] points;
    private int destPoint = 0;
    private NavMeshAgent agent;

    public bool _followingPlayer = false;

    private PlayerController _player = null;

    [SerializeField] private float _timeToLoseInterest = 3f;
    private float _timer;

    [SerializeField] private Animator _anim = null;

    [SerializeField] private bool _isEthereal = false;


    void Start () {
        agent = GetComponent<NavMeshAgent>();

        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).
        agent.autoBraking = false;

        GotoNextPoint();

        _anim.SetBool("IsEthereal", _isEthereal);
    }


    void GotoNextPoint() {
        // Returns if no points have been set up
        if (points.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;
    }


    void Update () {

        _anim.SetFloat("moveSpeed", Mathf.Abs(agent.velocity.magnitude));

        // Choose the next destination point when the agent gets
        // close to the current one.
        if(!_followingPlayer)
        {
            if (!agent.pathPending && agent.remainingDistance < 0.5f)
                GotoNextPoint();
        }
        else if(_player != null)
        {
            agent.destination = _player.isInEtherealForm ? _player.lifelessBody.transform.position : _player.transform.position;

            if(_isEthereal && _player.isInEtherealForm)
            {
                agent.destination = _player.transform.position;
            }

            _timer -= Time.deltaTime;

            if(_timer <= 0)
            {
                _followingPlayer = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            _player = other.GetComponent<PlayerController>();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if(_followingPlayer)
            {
                _timer = _timeToLoseInterest;
            }
            else
            {
                RaycastHit hit;
                Debug.DrawRay(transform.position, (_player.transform.position - transform.position)*1.5f);
                if(Physics.Raycast(transform.position, (_player.transform.position - transform.position)*1.5f, out hit, 10f))
                {
                    if(hit.transform.CompareTag("Player"))
                    {
                        _followingPlayer = true;
                    }
                }
            }
        }
    }
    
}
