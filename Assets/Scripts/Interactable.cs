﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    public bool _isBeingInteractedWith = false;
    
    abstract public bool Interact();

    abstract public void AbortInteraction();

    abstract public void Move(float y, float x);
}
