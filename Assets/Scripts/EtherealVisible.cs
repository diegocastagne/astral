﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EtherealVisible : MonoBehaviour
{
    public static List<EtherealVisible> list;
    public bool isVisible = true;

    private Renderer[] _renderers = null;
    private TextMeshProUGUI[] _texts = null;

    private void OnEnable()
    {
        if(list == null)
            list = new List<EtherealVisible>();
            
        list.Add(this);
    }

    private void Awake()
    {
        _renderers = GetComponentsInChildren<Renderer>();
        _texts = GetComponentsInChildren<TextMeshProUGUI>();
    }

    public void SwapVisibility()
    {
        if(gameObject.layer == 8)
            gameObject.layer = 9;
        else
            gameObject.layer = 8;

        if(isVisible)
        {
            foreach (var render in _renderers)
            {
                render.enabled = false;
            }
            foreach (var text in _texts)
            {
                text.enabled = false;
            }
            isVisible = false;
        }
        else
        {
            foreach (var render in _renderers)
            {
                render.enabled = true;
            }
            foreach (var text in _texts)
            {
                text.enabled = true;
            }
            isVisible = true;
        }
    }
}
