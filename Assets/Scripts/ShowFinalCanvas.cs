﻿
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ShowFinalCanvas : MonoBehaviour
{
    [SerializeField] private Image fadeOut = null;
    [SerializeField] private GameObject menuButton = null;

    private void Start()
    {
        StartCoroutine(FadeIn());
        StartCoroutine(CountDown(21.5f));
    }

    private IEnumerator CountDown(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        float duration = 0.3f;
        float normalizedTime = 0;
        while(normalizedTime <= 1f)
        {
            fadeOut.color = new Color(0f, 0f, 0f, normalizedTime);
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }

        fadeOut.color = new Color(0f, 0f, 0f, 1f);
        menuButton.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        //Time.timeScale = 0f;
        // gameObject.layer = 12;
        // lifelessBody.layer = 12;
    }

    private IEnumerator FadeIn()
    {
        float duration = 0.5f;
        float normalizedTime = 0;
        while(normalizedTime <= 1f)
        {
            fadeOut.color = new Color(0f, 0f, 0f, 1 - normalizedTime);
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }

        fadeOut.color = new Color(0f, 0f, 0f, 0f);
    }
}
