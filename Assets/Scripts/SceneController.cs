﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
  
    // load a scene
    public void LoadScene(string sceneName)
    {
        if (EtherealPenetrable.list != null && EtherealVisible.list != null)
        {
            EtherealPenetrable.list.Clear();
            EtherealVisible.list.Clear();
        }
        SceneManager.LoadScene(sceneName);
    }

    // quit
    public void Quit()
    {
        Debug.Log("Quitting!");
        Application.Quit();
    }

    public void ReloadCurrentScene()
    {
        if (EtherealPenetrable.list != null && EtherealVisible.list != null)
        {
            EtherealPenetrable.list.Clear();
            EtherealVisible.list.Clear();
        }

        Scene scene = SceneManager.GetActiveScene(); 
        SceneManager.LoadScene(scene.name);

        Time.timeScale = 1f;
    }

}
