﻿
using UnityEngine;

public class SoundSlidingDoor : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource = null;
    [SerializeField] private AudioClip[] audioClips = { };

    [Header("Pitch Randomization")]
    [SerializeField] private bool randomizePitch = false;
    [Range(-3.0f, 3.0f)]
    [SerializeField] private float minimumPitch = 0.0f;
    [Range(-3.0f, 3.0f)]
    [SerializeField] private float maximumPitch = 0.0f;

    void Awake()
    {
        ChangeClip(audioClips[0]);
    }

    public void PlayOpen()
    {
        if (randomizePitch)
        {
            float randomPitch = Random.Range(minimumPitch, maximumPitch);
            _audioSource.pitch = randomPitch;
        }


        if (_audioSource.isPlaying) return;

        ChangeClip(audioClips[0]);
        
        _audioSource.Play();
    }

    public void PlayClose()
    {
        if (randomizePitch)
        {
            float randomPitch = Random.Range(minimumPitch, maximumPitch);
            _audioSource.pitch = randomPitch;
        }



        if (_audioSource.isPlaying) return;

        ChangeClip(audioClips[1]);
        
        _audioSource.Play();
    }

    public void StopMusic()
    {
        _audioSource.Stop();
    }

    public void ChangeClip(AudioClip newClip)
    {
        _audioSource.clip = newClip;
    }
}
