﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _pauseMenu = null;
    [SerializeField] private GameObject _optionsMenu = null;
    [SerializeField] private PlayerController _player = null;

    [SerializeField] AudioMixerSnapshot paused = null;
	[SerializeField] AudioMixerSnapshot astral = null;
	[SerializeField] AudioMixerSnapshot physical = null;

    private AudioMixerSnapshot temp = null;

    private void Start()
    {
        SwapVisibility();
    }

    public void SwapObjects()
    {
        SwapVisibility();
        SwapPenetrable();
    }

    private void SwapVisibility()
    {
        if(EtherealVisible.list != null)
        {
            foreach(var element in EtherealVisible.list)
            {
                element.SwapVisibility();
            }
        }
    }

    private void SwapPenetrable()
    {
        if(EtherealPenetrable.list != null)
        {
            foreach(var element in EtherealPenetrable.list)
            {
                element.SwapLayers();
            }
        }
    }

    public void Pause()
    {
        Time.timeScale = 0f;
        _pauseMenu.SetActive(true);
        _player.paused = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        paused.TransitionTo(0.01f);
    }

    public void Resume()
    {
        Time.timeScale = 1f;
        _pauseMenu.SetActive(false);
        _optionsMenu.SetActive(false);
        _player.paused = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        if(_player.isInEtherealForm)
            astral.TransitionTo(0.01f);
        else
            physical.TransitionTo(0.01f);
    }
}
