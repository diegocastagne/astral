﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingTile : Interactable
{
    [SerializeField] private GameObject _instructionsDisplay = null; 
    
    // Here goes handling the interaction
    override public bool Interact()
    {
        if(!_instructionsDisplay.activeSelf)
        {
            _instructionsDisplay.SetActive(true); 
        }

        _isBeingInteractedWith = true;

        return true;
    }

    override public void AbortInteraction()
    {
        if(_instructionsDisplay.activeSelf)
        {
            _instructionsDisplay.SetActive(false); 
        }

        _isBeingInteractedWith = false;
    }

    override public void Move(float y, float x)
    {
        transform.Rotate(Vector3.up, x * Time.deltaTime * 100f);
    }

    private void OnDisable()
    {
        _isBeingInteractedWith = false;
    }
}
