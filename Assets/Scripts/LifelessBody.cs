﻿using UnityEngine;

public class LifelessBody : MonoBehaviour
{
    [SerializeField] private PlayerController _player = null;
    [SerializeField] private InputManager _input = null;

    public bool playerHasExited = false;

    private void OnCollisionEnter(Collision other)
    {
        if(other.collider.CompareTag("Enemy"))
            _player.Die();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player") && playerHasExited)
        {
            _input.Swap();
            //Debug.Log(other.gameObject.tag + " went back to their body");
            //playerHasExited = false;
        }

        if(other.CompareTag("LifelessKiller"))
        {
            _player.Die();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            //Debug.Log(other.gameObject.tag + " exited their body");
            playerHasExited = true;
        }
    }
}
