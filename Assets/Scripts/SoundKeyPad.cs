﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundKeyPad : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource = null;
    [SerializeField] private AudioClip[] audioClips = { };

    public void PlayCorrect()
    {
        if (_audioSource.isPlaying) return;

        // Audio Clip randomization
        ChangeClip(audioClips[0]);
        
        _audioSource.Play();
    }

    public void PlayIncorrect()
    {
        if (_audioSource.isPlaying) return;

        // Audio Clip randomization
        ChangeClip(audioClips[1]);
        
        _audioSource.Play();
    }

    public void StopMusic()
    {
        _audioSource.Stop();
    }

    public void ChangeClip(AudioClip newClip)
    {
        _audioSource.clip = newClip;
    }
}
