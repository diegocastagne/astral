﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : Interactable
{
    [SerializeField] private LayerMask _placeHolderLayerMask = default;

    public bool isBeingHeld = false;
    public bool canBePlaced = false;

    private Rigidbody _rbd = null;

    private Collider _collider = null;

    private GameObject _closestPlaceHolder = null;

    void Start()
    {
        _rbd = GetComponent<Rigidbody>();
        _collider = GetComponent<BoxCollider>();
    }

    void FixedUpdate()
    {
        var collisions = Physics.OverlapSphere(transform.position, 1f, _placeHolderLayerMask);

        if(collisions.Length > 0)
        {
            _closestPlaceHolder = collisions[0].gameObject;
            canBePlaced = true;
        }
        else
        {
            _closestPlaceHolder = null;
            canBePlaced = false;
        }        
    }


    public override bool Interact()
    {
        _rbd.constraints = RigidbodyConstraints.None;

        isBeingHeld = !isBeingHeld;

        if(!isBeingHeld && _closestPlaceHolder != null)
        {
            transform.position = _closestPlaceHolder.transform.position;
            transform.rotation = _closestPlaceHolder.transform.rotation;

            _rbd.constraints = RigidbodyConstraints.FreezeAll;
        }

        return false;
    }

    public override void AbortInteraction()
    {
        //isBeingHeld = false;
        //throw new System.NotImplementedException();
    }


    public override void Move(float y, float x){}

}
