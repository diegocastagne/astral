﻿
using UnityEngine;

public class ActivateOptions : MonoBehaviour
{
    [SerializeField] private GameObject _options = null;

    public void Activate()
    {
        _options.SetActive(true);
    }

    public void Deactivate()
    {
        _options.SetActive(false);
    }
}
