﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : Interactable
{
    [SerializeField] private AudioSource _clickSound = null;
    [SerializeField] private AudioSource _electricSound = null;

    [SerializeField] private Cable _generatorCable = null;
    [SerializeField] private Cable _receivingCable = null;

    public bool _isOn = false;

    [SerializeField] private bool _isRepeater = false;

    private void Awake()
    {
        _generatorCable.thisCableBelongsToTheGenerator = true;
    }

    override public bool Interact()
    {
        if(_isRepeater && !_receivingCable.hasElectricity) return false;

        if(_isOn)
        {
            _isOn = false;
            _generatorCable.hasElectricity = false;
            _clickSound.Stop();
            _electricSound.Stop();
        }
        else
        {
            _isOn = true;
            _generatorCable.hasElectricity = true;
            _clickSound.Play();
            _electricSound.Play();
        }
        
        return false;
    }

    override public void AbortInteraction()
    {
        // shouldn't need to hit Esc lol
    }

    public override void Move(float y, float x)
    {
    }
}
