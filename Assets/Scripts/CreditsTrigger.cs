﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CreditsTrigger : MonoBehaviour
{
    [SerializeField] private string _sceneToLoad = null;

    [SerializeField] Image fade = null;
    [SerializeField] Image secondFade = null;
    [SerializeField] PlayerController _player = null;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(FadeOut());
            //EtherealPenetrable.list.Clear();
            //EtherealVisible.list.Clear();
            
            _player.lost = true;
        }
    }

    private IEnumerator FadeOut()
    {
        float duration = 5f;
        float normalizedTime = 0;
        while(normalizedTime <= 1f)
        {
            fade.color = new Color(1f, 1f, 1f, normalizedTime);
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }
        fade.color = new Color(1f, 1f, 1f, 1f);

        yield return new WaitForSeconds(0.5f);

        duration = 3f;
        normalizedTime = 0;
        while(normalizedTime <= 1f)
        {
            secondFade.color = new Color(0f, 0f, 0f, normalizedTime);
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }
        secondFade.color = new Color(0f, 0f, 0f, 1f);
        SceneManager.LoadScene(_sceneToLoad);
    }
}
