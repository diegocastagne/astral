﻿using UnityEngine;
 using System.Collections;
 
 public class MusicPlayer : MonoBehaviour
 {
    [SerializeField] private AudioSource _audioSource = null;

    private static MusicPlayer instance = null;
    public static MusicPlayer Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void PlayMusic()
    {
        if (_audioSource.isPlaying) return;
        _audioSource.Play();
    }

    public void StopMusic()
    {
        _audioSource.Stop();
    }

    public void ChangeClip(AudioClip newClip)
    {
        _audioSource.clip = newClip;
    }
}
