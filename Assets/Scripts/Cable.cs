using System;
using System.Collections.Generic;
using UnityEngine;

public class Cable : MonoBehaviour
{
    [SerializeField] private Material _onMaterial = null;
    [SerializeField] private Material _offMaterial = null;
    [SerializeField] private List<Cable> _connectedCables = null;
    public bool hasElectricity = false;
    public bool thisCableBelongsToTheGenerator = false;

    private bool _checkingConnection = false;

    private void Awake()
    {
        _connectedCables = new List<Cable>();
    }

    private void Update()
    {
        if(!thisCableBelongsToTheGenerator)
        {
            this.hasElectricity = false;

            foreach (var cable in _connectedCables)
            {
                if(cable.hasElectricity)
                    this.hasElectricity = true;
            }
        }


        if(GetComponentInChildren<MeshRenderer>() != null)
        {
            if(hasElectricity)
            {
                GetComponentInChildren<MeshRenderer>().material = _onMaterial;
            }
            else
            {
                GetComponentInChildren<MeshRenderer>().material = _offMaterial;
            }
        }
    }

    private void FixedUpdate()
    {
        if(_checkingConnection)
        {
            //Disconnect();
            _checkingConnection = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        var otherCable = other.gameObject.GetComponent<Cable>();
        if(otherCable != null)
        {
            if(!_connectedCables.Contains(otherCable))
            {
                _connectedCables.Add(otherCable);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var otherCable = other.gameObject.GetComponent<Cable>();
        if(otherCable != null)
        {
            _connectedCables.Remove(otherCable);
        }
        //otherCable.CheckConnection();
    }

    // private void CheckConnection()
    // {
    //     _checkingConnection = true;
    // }

    // private void Disconnect()
    // {
    //     if(!thisCableBelongsToTheGenerator)
    //     {
    //         hasElectricity = false;
    //         foreach (var cable in _connectedCables)
    //         {
    //             cable.Disconnect();
    //         }
    //     }
    // }
}
