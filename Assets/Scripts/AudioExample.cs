// January 22nd, 2019
// Matheus Vilano (Matt) - msouza@vfs.com
// Team Project

using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class AudioExample : MonoBehaviour
{
    [Header("Audio Source")]
    [SerializeField] private AudioSource audioSource = null;
    [SerializeField] private AudioClip[] audioClips = { };

    [Header("Volume Randomization")]
    [SerializeField] private bool randomizeVolume = false;
    [Range(0.0f, 1.0f)]
    [SerializeField] private float minimumVolume = 1.0f;
    [Range(0.0f, 1.0f)]
    [SerializeField] private float maximumVolume = 1.0f;

    [Header("Pitch Randomization")]
    [SerializeField] private bool randomizePitch = false;
    [Range(-3.0f, 3.0f)]
    [SerializeField] private float minimumPitch = 0.0f;
    [Range(-3.0f, 3.0f)]
    [SerializeField] private float maximumPitch = 0.0f;

    [Header("Audio Mixer")]
    [SerializeField] private AudioMixer audioMixer = null;
    [SerializeField] private AudioMixerSnapshot[] snapshots = null;
    [SerializeField] private float[] weights = {};
    [SerializeField] private float transitionTime = 1.0f;

    [ContextMenu("Trigger Sound")]
    private void AudioSourceExample()
    {
        // Audio Clip randomization
        {
            int randomClip = Random.Range(0, audioClips.Length);
            audioSource.clip = audioClips[randomClip];
            Debug.Log($"{gameObject.ToString()} | Variation #{randomClip}");
        }

        // Volume randomization
        if (randomizeVolume)
        {
            float randomVolume = Random.Range(minimumVolume, maximumVolume);
            audioSource.volume = randomVolume;
            Debug.Log($"{gameObject.ToString()} | Volume at {randomVolume}");
        }

        // Pitch randomization
        if (randomizePitch)
        {
            float randomPitch = Random.Range(minimumPitch, maximumPitch);
            audioSource.pitch = randomPitch;
            Debug.Log($"{gameObject.ToString()} | Pitch at {randomPitch}");
        }
    }

    private void AudioMixerExample()
    {
        audioMixer.TransitionToSnapshots(snapshots, weights, transitionTime);
    }
}
